export default function untilAfter (pattern) {
  return this.includes(pattern)
    ? this.substring(0, this.indexOf(pattern) + pattern.length)
    : this
}
